import { Component, OnInit, EventEmitter, Output,OnDestroy } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Subscription } from 'rxjs/Subscription';
import { GetBooksService } from '../../services/getbooks.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header-component.html',
  styleUrls: ['./header-component.css']
})
export class HeaderComponent implements OnInit , OnDestroy {
  @Output() sidenavToggle = new EventEmitter<void>();
  isAuth = false;
  authSubscription: Subscription;
  userName:string="";
  

  constructor(private authService: AuthService, private bookService: GetBooksService,private router:Router) { }

  ngOnInit() {
    this.authSubscription = this.authService.authChange.subscribe(authStatus => {
      this.isAuth = authStatus;
      
    });
    
    this.authService.nameChange.subscribe(data =>{
      this.userName = data;
    })
 
      
   
     
 

  }
  onLogout()
  {
    this.authService.logout();
    
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
  }
  // onLogout() {
  //   this.authService.logout();
  // }
  ngOnDestroy() {
    this.authSubscription.unsubscribe();
  }


}
